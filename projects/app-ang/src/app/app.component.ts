import { AfterViewInit, Component, ElementRef, OnInit, ViewChild, ViewContainerRef, Optional } from '@angular/core';
import { RoomsComponent } from './rooms/rooms.component';
import { LoggerService } from './logger.service'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Hotel ß';

  @ViewChild('name', {static: true}) name! : ElementRef;

  constructor(@Optional private loggerService: LoggerService){}

  ngOnInit(): void {
    this.loggerService?.Log('AppComponent.ngOnInit()')
      this.name.nativeElement.innerText = "Blues Rezidans"
  }
  // role = "Admin"
  // @ViewChild('user', {read: ViewContainerRef}) vcr!: ViewContainerRef

  // ngAfterViewInit() {
  //   const componentRef = this.vcr.createComponent(RoomsComponent)
  //   componentRef.instance.numberOfRooms = 50
  // }
}
