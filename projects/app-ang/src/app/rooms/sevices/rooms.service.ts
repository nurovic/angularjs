import { Injectable } from '@angular/core';
import { Room, RoomList } from '../rooms';

@Injectable({
  providedIn: 'root',
})
export class RoomsService {
  roomList: RoomList[] = [
    {
      roomNumber: 11,
      roomType: 'Deluxe Room',
      amenities: 'Air Conditioner, Free Wi-Fi',
      price: 500,
      photos:
        'https://news.airbnb.com/wp-content/uploads/sites/4/2019/06/PJM020719Q202_Luxe_WanakaNZ_LivingRoom_0264-LightOn_R1.jpg?fit=2500%2C1666',
      checkinTime: new Date('11-November-2021'),
      checkoutTime: new Date('12-November-2021'),
      rating: 4.534243,
    },
    {
      roomNumber: 12,
      roomType: 'Deluxe Room',
      amenities: 'Air Conditioner, Free Wi-Fi',
      price: 2000,
      photos:
        'https://news.airbnb.com/wp-content/uploads/sites/4/2019/06/PJM020719Q202_Luxe_WanakaNZ_LivingRoom_0264-LightOn_R1.jpg?fit=2500%2C1666',
      checkinTime: new Date('24-November-2021'),
      checkoutTime: new Date('30-November-2021'),
      rating: 4,
    },
    {
      roomNumber: 14,
      roomType: 'Private Room',
      amenities: 'Air Conditioner, Free Wi-Fi',
      price: 2500,
      photos:
        'https://news.airbnb.com/wp-content/uploads/sites/4/2019/06/PJM020719Q202_Luxe_WanakaNZ_LivingRoom_0264-LightOn_R1.jpg?fit=2500%2C1666',
      checkinTime: new Date('18-November-2021'),
      checkoutTime: new Date('30-November-2021'),
      rating: 5,
    },
  ];
  constructor() {}

  getRooms() {
    return this.roomList
  }
}
