import { AfterViewChecked, AfterViewInit, Component, DoCheck, OnInit, QueryList, ViewChild, ViewChildren, SkipSelf } from '@angular/core';
import { Room, RoomList } from './rooms';
import { HeaderComponent } from '../header/header.component';
import { RoomsService } from './sevices/rooms.service'
@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit, DoCheck, AfterViewInit, AfterViewChecked{

  hotelName =  "Hiltown otel"
  numberOfRooms = 12
  hideRooms = false

  selectedRoom!: RoomList
  
  rooms : Room = {
    totalRooms: 20,
    availableRooms: 10,
    bookedRooms: 5
  }
 
  roomList : RoomList[] = []

  @ViewChild(HeaderComponent) headerComponent! : HeaderComponent; 

  @ViewChildren(HeaderComponent) headerChildrenComponent!: QueryList<HeaderComponent> 

  // roomService = new RoomService()

  constructor(@SkipSelf() private roomsService: RoomsService) {}

  ngOnInit(): void {
    this.roomList = this.roomsService.getRooms()
  }
  title = 'Room List'

  ngDoCheck(): void {
"      5.43.02"
  }
  toggle() {
    this.hideRooms = !this.hideRooms
    this.title = 'Rooms List'

  }

  selectRoom(room: RoomList){
    this.selectedRoom = room
  }

  addRoom() {
    const room: RoomList = {
      roomNumber: 51,
      roomType: 'Deluxe Room',
      amenities:'Air conditioner, Free Wi-Fi, Kitchen',
      price:600,
      photos:"https://news.airbnb.com/wp-content/uploads/sites/4/2019/06/PJM020719Q202_Luxe_WanakaNZ_LivingRoom_0264-LightOn_R1.jpg?fit=2500%2C1666",
      checkinTime: new Date('11-Nov-2021'),
      checkoutTime: new Date('14-Nov-2021'),
      rating:4.6
    }
    // this.roomList.push(room)

    this.roomList = [...this.roomList, room]
  }

  ngAfterViewInit(): void {
      this.headerComponent.title = "Rooms View"

      this.headerChildrenComponent.last.title = "Last Title"
  }

  ngAfterViewChecked(): void {
      
  }
}
